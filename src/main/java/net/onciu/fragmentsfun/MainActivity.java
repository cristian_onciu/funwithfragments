package net.onciu.fragmentsfun;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.*;
import android.widget.TextView;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;
import net.onciu.fragmentsfun.fragments.Fragment1;
import net.onciu.fragmentsfun.fragments.UserPhotoListFragment;

public class MainActivity extends ActionBarActivity implements GestureListener.OnGestureCallback {

    GestureDetector gestureDetector;

    public static View.OnTouchListener gestureListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init Parse
        Parse.initialize(this, "7mMmeEH8JXiMNBi0nOdOKdCjlMGbI3YSrxmBmwJa", "Tjb3YIBrtBvzW4bXVcmkcDVUYWBeGEf2AKOU9WqR");
        // Check to see if there's a parse user in the cache
        if (ParseUser.getCurrentUser() == null) {
            // SignIn SamFest user
            ParseUser.logInInBackground("lokken", "password", new LogInCallback() {

                @Override
                public void done(ParseUser user, ParseException e) {
                    if (user != null) {
                        // Hooray! The user is logged in.
                        Log.v("PARSE_USER", "Logged in");
                    } else {
                        // Signup failed. Look at the ParseException to see what
                        // happened.
                        Log.v("PARSE_USER", "Cannot log in");
                    }
                }
            });
        }

        // Gesture detection
        gestureDetector = new GestureDetector(this, new GestureListener(this));
        gestureListener = new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        };

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new InitialFragment(), InitialFragment.TAG)
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @Override
    public void gestureDone(int gesture) {

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (fragmentManager.findFragmentByTag(InitialFragment.TAG) != null) {
            if (gesture == GestureListener.LEFT) {
                Fragment1 fragment1 = new Fragment1();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                fragmentTransaction.replace(R.id.container, fragment1, Fragment1.TAG).commit();
            }
        } else if (fragmentManager.findFragmentByTag(Fragment1.TAG) != null) {
            if (gesture == GestureListener.RIGHT) {
                InitialFragment placeholderFragment = new InitialFragment();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                fragmentTransaction.replace(R.id.container, placeholderFragment, InitialFragment.TAG).commit();
            } else if (gesture == GestureListener.LEFT) {
                ListFragment listFragment = new UserPhotoListFragment();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                fragmentTransaction.replace(R.id.container, listFragment, UserPhotoListFragment.TAG).commit();
            }
        } else if (fragmentManager.findFragmentByTag(UserPhotoListFragment.TAG) != null) {
            if (gesture == GestureListener.RIGHT) {
                Fragment1 fragment1 = new Fragment1();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                fragmentTransaction.replace(R.id.container, fragment1, Fragment1.TAG).commit();
            }
        }
    }

    @Override
    public void onBackPressed(){
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class InitialFragment extends Fragment {

        public static final String TAG = "InitialFragment";

        public InitialFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            assert rootView != null;
            rootView.setOnTouchListener(gestureListener);
            TextView textView = (TextView) rootView.findViewById(R.id.textView);
            textView.setText("Inner Fragment");
            return rootView;
        }


    }

}
