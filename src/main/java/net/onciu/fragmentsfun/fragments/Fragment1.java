package net.onciu.fragmentsfun.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import net.onciu.fragmentsfun.MainActivity;
import net.onciu.fragmentsfun.R;

/**
 * Created with IntelliJ IDEA.
 * User: Cristian Onciu
 * Date: 2/13/14
 * Time: 8:49 PM
 */
public class Fragment1 extends Fragment {
    public static final String TAG="Fragment1";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        MainActivity mainActivity= (MainActivity) getActivity();
        rootView.setOnTouchListener(mainActivity.gestureListener);
        TextView textView= (TextView) rootView.findViewById(R.id.textView);
        textView.setText("Fragment1");
        return rootView;
    }
}
