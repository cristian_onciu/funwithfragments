package net.onciu.fragmentsfun.fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import net.onciu.fragmentsfun.R;
import com.parse.ParseQueryAdapter;
import net.onciu.fragmentsfun.MainActivity;
import net.onciu.fragmentsfun.adapters.PhotoAdapter;
import net.onciu.fragmentsfun.models.UserPhoto;

/**
 * Created with IntelliJ IDEA.
 * User: Cristian Onciu
 * Date: 2/14/14
 * Time: 6:45 PM
 */
public class UserPhotoListFragment extends ListFragment {
    public static final String TAG="UserPhotoListFragment";
    private ParseQueryAdapter<UserPhoto> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        rootView.setOnTouchListener(MainActivity.gestureListener);
        TextView textView= (TextView) rootView.findViewById(R.id.textViewPageNr);
        textView.setText("1");
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new PhotoAdapter(getActivity());
        setListAdapter(adapter);
    }
}
